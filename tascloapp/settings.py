"""
Django settings for tascloapp project.

Generated by 'django-admin startproject' using Django 1.10.4.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.10/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'jd5sk&0uoke0*l)5mh^a8$-3lgu*$cf!i&_#*m9$9+qjlx@nox'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['tasclo-env.gwv6bbv49a.eu-west-1.elasticbeanstalk.com', # AWS
                 'localhost', # local run
                 '127.0.0.1', # local run as well
                 ]


FACEBOOK_APP_ID='1167148423340133'
FACEBOOK_API_SECRET='8b72c55114eeb8187afc1394912469a6'
SOCIALACCOUNT_QUERY_EMAIL = True
LOGIN_REDIRECT_URL = '/'
INSTALLED_APPS = [
    'home',
    'events',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
                  
    'django.contrib.sites',
                  
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'tascloapp.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'tascloapp.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases


if 'RDS_DB_NAME' in os.environ:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.environ['RDS_DB_NAME'],
            'USER': os.environ['RDS_USERNAME'],
            'PASSWORD': os.environ['RDS_PASSWORD'],
            'HOST': os.environ['RDS_HOSTNAME'],
            'PORT': os.environ['RDS_PORT'],
        }
    }
else:
    # Figure out how to access postgresql locally
    DATABASES = {
	'default': {
	    'ENGINE': 'django.db.backends.postgresql_psycopg2',
	    'NAME': 'tasclo',
	    'USER': 'tasclouser',
	    'PASSWORD': 'tascloadmin',
	    'HOST': 'localhost',
	    'PORT': '',
	}
    }
    #DATABASES = {
    #    'default': {
    #        'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #        'NAME': 'ebdb',
    #        'USER': 'master',
    #        'PASSWORD': 'tasclomaster',
    #        'HOST': 'aamk5t4mj4vh19.chpimxmbsvfv.eu-west-1.rds.amazonaws.com',
    #        'PORT': '5432',
    #    }
    #}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'static'

AUTHENTICATION_BACKENDS = (
                           # Needed to login by username in Django admin, regardless of `allauth`
                           'django.contrib.auth.backends.ModelBackend',
                           
                           # `allauth` specific authentication methods, such as login by e-mail
                           'allauth.account.auth_backends.AuthenticationBackend',
                           )

SITE_ID = 1
