from django.shortcuts import render
from django.views.generic import DetailView
from django.http import HttpResponseForbidden

from .models import Event
from .models import RegisteredUser

# register a new user to an event. pk is event id from url
def adduser(request, pk):

    user = request.user
    event = Event.objects.all().filter(id=pk)[0]

    if user.is_authenticated:
        # register the user if they are not already
        if not RegisteredUser.objects.filter(event=event, user=user).exists():
            newuser = RegisteredUser(event=event, user=user)
            newuser.save()

        context = { 'event': event }

        return render(request, 'events/congrats.html', context)

    else:
        return HttpResponseForbidden()


class IndiEventView(DetailView):
    """Add extra context data depending detialing if the  user is already
    registered"""

    model = Event
    template_name = "events/indi_event.html"

    def get_context_data(self, **kwargs):

        # Call the base implementation first to get a context
        context = super(IndiEventView, self).get_context_data(**kwargs)

        # Add whether the user is already registered!
        user = self.request.user
        event = Event.objects.all().filter(id=self.kwargs['pk'])[0]
        if user.is_authenticated and RegisteredUser.objects.filter(
                event=event, user=user).exists():
            already_registered = True
        else:
            already_registered = False
        context['already_registered'] = already_registered

        return context
