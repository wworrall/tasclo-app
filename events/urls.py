from django.conf.urls import url, include
from django.views.generic import ListView
from events.models import Event
from . import views

urlpatterns = [
        # Events previews
	url(r'^$', ListView.as_view(
            queryset=Event.objects.all().order_by("deadline"),
            template_name="events/events.html")),

        # Individual Event view
	url(r'^(?P<pk>\d+)$', views.IndiEventView.as_view()),

        # Add a new user
        url(r'(?P<pk>\d+)/adduser/$', views.adduser),
	]
