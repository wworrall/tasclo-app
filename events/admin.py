from events.models import Event
from events.models import RegisteredUser
from django.contrib import admin
admin.site.register(Event)
admin.site.register(RegisteredUser)
