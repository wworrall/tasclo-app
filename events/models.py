from django.db import models
from django.contrib.auth.models import User

class Event(models.Model):
    name = models.CharField(max_length=300)
    no_tickets = models.IntegerField()
    ticket_price = models.DecimalField(max_digits=7, decimal_places=2)
    deadline = models.DateTimeField()
    date_of_event = models.DateTimeField()
    type_of_event = models.CharField(max_length=300)
    description_of_event = models.TextField()
    location_of_event = models.CharField(max_length=1000)
    url_of_event = models.URLField()
    event_image = models.URLField()

    def __str__(self):
        return self.name

class RegisteredUser(models.Model):
   event = models.ForeignKey(Event, default=1, on_delete=models.CASCADE)
   user = models.ForeignKey(User, default=1)

   def __str__(self):
       return str(self.user.id)
