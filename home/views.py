from django.shortcuts import render

def index(request):
    return render(request, 'home/testinghome.html')

def contact(request):
	return render(request, 'home/basic.html', {'content':['If you would like to contact us, please email', 'tasclo@gmail.com']})